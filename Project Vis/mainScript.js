//AUTHOR - CHARLIE MURPHY 2017

// readies the DOM queue
$(document).ready(function() {

  // clears storage on refresh
  localStorage.clear();
  var clone = $('#menu')

  // disables pause button before conquest is played
  disableButton('#pause')

  // renders map on document load
  $(function() {

    // Initial year and title variables
    var newYear = '500',
      newYear2 = '  BCE',
      newTitle = 'Roman Republic'

    // grabs JSON data from file/server
    // moved data to JSON to clean this code
    var jsonMap;
    $.getJSON('mapData.json', function(result) {
      jsonMap = result;

      // Chart definition
      $('#container').highcharts('Map', {
        title: {
          text: newTitle,
          style: {
            fontWeight: 'bold',
            fontSize: '30px',
            fontFamily: 'Roman SD'
          }
        },
        subtitle: {
          text: '500 BCE',
          style: {
            fontSize: '15px'
          }
        },
        plotOptions: {
          series: {
            cursor: 'pointer',
            point: {
              events: {

                // allows user to click province to skip ahead
                click: function clickEvent(event) {
                  clickStart(this.index)

                }
              }
            }
          }
        },
        colorAxis: {
          min: 0,
          max: 100,
          minColor: 'gray',
          maxColor: 'red'
        },
        legend: {
          enabled: false
        },
        tooltip: {
          backgroundColor: 'none',
          borderWidth: 0,
          shadow: false,
          useHTML: true,
          padding: 0,
          pointFormat: '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Vexilloid_of_the_Roman_Empire.svg/245px-Vexilloid_of_the_Roman_Empire.svg.png" width=35px height=40px/><br><span style="font-size:15px">{point.name}</span><br>' +
            '<span style="font-size:12px">{point.battles}</span>',
          headerFormat: '',

          // Credit @ http://ahumbleopinion.com/customizing-highcharts-tooltip-positioning/ -- modified the code to fit requirements
          positioner: function(labelWidth, labelHeight, point) {
            var tooltipX, tooltipY;
            if (point.plotX + this.chart.plotLeft < labelWidth && point.plotY + labelHeight > this.chart.plotHeight) {
              tooltipX = this.chart.plotLeft;
              tooltipY = this.chart.plotTop + this.chart.plotHeight - 2 * labelHeight - 10;
            } else {
              tooltipX = this.chart.plotLeft + 225;
              tooltipY = this.chart.plotTop + this.chart.plotHeight - labelHeight - 20;
            }
            return {
              x: tooltipX,
              y: tooltipY
            };
          }
        },
        series: [{
          type: 'map',
          data: jsonMap

        }]
      })
    })
  })

  // renders chart on document load
  $(function() {
    var jsonData;

    // grabs JSON from JSON file in directory
    $.getJSON('chartData.json', function(result) {
      jsonData = result;


      // renders chart to right side div
      $('#rightSide').highcharts({
        chart: {
          type: 'area',
          animation: {
            duration: 1000
          },
        },
        title: {
          text: 'Conflict Casualties'
        },

        subtitle: {
          text: 'Source: See documentation'
        },

        yAxis: {
          title: {
            text: 'Number of Casualties'
          }
        },
        xAxis: {
          categories: ['First Punic', 'Second Punic', 'Greek Subjugation', 'Third Punic War', 'Mithridatic Wars', 'Gallia Campaign', 'Caesar Civil War'],
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },

        plotOptions: {
          line: {
            dataLabels: {
              enabled: true,
            }
          },
          series: {
            pointStart: 0,
            connectNulls: true,
            fillOpacity: 0.3
          }
        },

        // loads json data gathered from start of function
        series: jsonData,


      });
    })
  })

  // in progress -- want map to turn purple when becomes empire
  function makeMapPurple() {

    // chart is the highchart map
    var chart = $('#container').highcharts()
    counter = localStorage.getItem('purpleNumber')

    $('html').css('background-color', 'purple')

    // iterates through the map
    for (var i = 0; i < counter; i++) {
      chart.series[0].data[i].update({
        "color": 'purple'
      }, true);
    }
  }


  // changes chart's data when called
  function changeChart(counter) {


    // chart && chart2 are the two highcharts
    var chart = $('#container').highcharts()
    var chart2 = $('#rightSide').highcharts()

    // empty arrays for chart data to be passed into
    var carthArray = []
    var romeArray = []
    var ponticArray = []
    var macedonArray = []
    var gallicArray = []
    var parthianArray = []

    // undefined vars for localStorage values to be passed to
    var changed1, changed2


    if (40 - counter < 40) { // 40 is the number of areas on the map

      // clears the chart if map is focused on non-related areas
      if (40 - counter >= 37) {
        for (var i = 0; i < 6; i++) {
          chart2.series[i].setData(carthArray)
        }
      }

      // begins if statement to check for keywords in the data
      if (chart.series[0].data[40 - counter].name.includes("First Punic")) { // checks for specific names of provinces

        // if matched, assigns appropriate data to var declared earlier
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if arrays are undefined then data is pushed
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          carthArray.push(changed1)
          romeArray.push(changed2)

          // sets data in localStorage to use later
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // changes chart data to the now filled arrays
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // if data already exists, it is retrieved
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes any data that should not be there due to map's state
          while (romeArray.length > 0) {
            romeArray.pop()
          }

          // appends data to the array
          romeArray.push(changed2)

          // reinserts the data into localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // applies data to the chart
          chart2.series[1].setData(romeArray)

          // does the same for next array
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          while (carthArray.length > 0) {
            carthArray.pop()
          }

          carthArray.push(changed1)
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          chart2.series[0].setData(carthArray)
        }

        // repeat for next keyword
      } else if (chart.series[0].data[40 - counter].name.includes("Second Punic")) {

        // assigns data
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // checks for pre-existing data
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // pushes data into arrays
          carthArray.push(changed1)
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // appends data to chart
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // if data exists, retrieve it
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes existing data if it is not appropriate
          while (romeArray.length > 1) {
            romeArray.pop()
          }

          // pushes data into array
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to the array
          chart2.series[1].setData(romeArray)

          // retrirves data from localStorage
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          // removes inappropriate data
          while (carthArray.length > 1) {
            carthArray.pop()
          }

          // pushes data into array
          carthArray.push(changed1)

          // saves data into localStorage for later usage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))

          // applies data to the chart
          chart2.series[0].setData(carthArray)
        }
        // checks for next keywords
      } else if (chart.series[0].data[40 - counter].name.includes("Macedonian Wars")) {

        // assigns retrieved data to vars
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if data is undefined
        if (localStorage.getItem('macedonArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrieves data from localStorage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 3) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart to array data
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (macedonArray.length > 3) {
            macedonArray.pop()
          }

          // push data to array
          macedonArray.push(null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('macedonArray', JSON.stringify(macedonArray))

          // sets chart data to array
          chart2.series[2].setData(macedonArray)
        }

        // repeats for next keywords
      } else if (chart.series[0].data[40 - counter].name.includes("Third Punic")) {

        // assigns data to declared vars
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if data is undefined
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // pushes data into array
          carthArray.push(changed1)
          romeArray.push(changed2)

          // saves data into localStorage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to array
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // otherwise retrieves data
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 3) {
            romeArray.pop()
          }

          // pushes data into array
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to array
          chart2.series[1].setData(romeArray)

          // retrireves data from localStorage
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          // removes inappropriate data
          while (carthArray.length > 3) {
            carthArray.pop()
          }

          // pushes data into array
          carthArray.push(null, changed1)

          // saves data to localStorage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))

          // sets data to array
          chart2.series[0].setData(carthArray)
        }


        // checks for next keywords
      } else if (chart.series[0].data[40 - counter].name.includes("Mithridatic")) {

        // assigns data to vars
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if data is undefined
        if (localStorage.getItem('ponticArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 4) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (ponticArray.length > 3) {
            ponticArray.pop()
          }

          // pushes data to array
          ponticArray.push(null, null, null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('ponticArray', JSON.stringify(ponticArray))

          // sets chart data to array
          chart2.series[3].setData(ponticArray)
        }



        // checks for next keywords
      } else if (chart.series[0].data[40 - counter].name.includes("Gallia")) {

        // assigns data to vars
        changed1 = parseInt(chart.series[0].data[40 - counter].batData1)
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if data is undefined
        if (localStorage.getItem('gallicArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 5) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (gallicArray.length > 5) {
            gallicArray.pop()
          }

          // pushes data to array
          gallicArray.push(null, null, null, null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('gallicArray', JSON.stringify(gallicArray))

          // sets chart data to array
          chart2.series[4].setData(gallicArray)
        }

      } else if (chart.series[0].data[40 - counter].name.includes("Caesar")) {

        // assigns data to vars
        changed2 = parseInt(chart.series[0].data[40 - counter].batData2)

        // if data is undefined
        if (localStorage.getItem('romeArray') != undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 6) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)
        }

      }
    }
  }

  // changes chart's data when called
  function clickChart(counter) {

    // chart && chart2 are the two highcharts
    var chart = $('#container').highcharts()
    var chart2 = $('#rightSide').highcharts()

    // empty arrays for chart data to be passed into
    var carthArray = []
    var romeArray = []
    var ponticArray = []
    var macedonArray = []
    var gallicArray = []
    var parthianArray = []

    // undefined vars for localStorage values to be passed to
    var changed1, changed2


    if (counter < 40) { // 40 is the number of areas on the map

      // clears the chart if map is focused on non-related areas
      if (counter >= 37) {
        for (var i = 0; i < 6; i++) {
          chart2.series[i].setData(carthArray)
        }
      }

      // begins if statement to check for keywords in the data
      if (chart.series[0].data[counter].name.includes("First Punic")) { // checks for specific names of provinces

        // if matched, assigns appropriate data to var declared earlier
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if arrays are undefined then data is pushed
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          carthArray.push(changed1)
          romeArray.push(changed2)

          // sets data in localStorage to use later
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // changes chart data to the now filled arrays
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // if data already exists, it is retrieved
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes any data that should not be there due to map's state
          while (romeArray.length > 0) {
            romeArray.pop()
          }

          // appends data to the array
          romeArray.push(changed2)

          // reinserts the data into localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // applies data to the chart
          chart2.series[1].setData(romeArray)

          // does the same for next array
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          while (carthArray.length > 0) {
            carthArray.pop()
          }

          carthArray.push(changed1)
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          chart2.series[0].setData(carthArray)
        }

        // repeat for next keyword
      } else if (chart.series[0].data[counter].name.includes("Second Punic")) {

        // assigns data
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // checks for pre-existing data
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // pushes data into arrays
          carthArray.push(changed1)
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // appends data to chart
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // if data exists, retrieve it
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes existing data if it is not appropriate
          while (romeArray.length > 1) {
            romeArray.pop()
          }

          // pushes data into array
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to the array
          chart2.series[1].setData(romeArray)

          // retrirves data from localStorage
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          // removes inappropriate data
          while (carthArray.length > 1) {
            carthArray.pop()
          }

          // pushes data into array
          carthArray.push(changed1)

          // saves data into localStorage for later usage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))

          // applies data to the chart
          chart2.series[0].setData(carthArray)
        }
        // checks for next keywords
      } else if (chart.series[0].data[counter].name.includes("Macedonian Wars")) {

        // assigns retrieved data to vars
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if data is undefined
        if (localStorage.getItem('macedonArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrieves data from localStorage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 3) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart to array data
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (macedonArray.length > 3) {
            macedonArray.pop()
          }

          // push data to array
          macedonArray.push(null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('macedonArray', JSON.stringify(macedonArray))

          // sets chart data to array
          chart2.series[2].setData(macedonArray)
        }

        // repeats for next keywords
      } else if (chart.series[0].data[counter].name.includes("Third Punic")) {

        // assigns data to declared vars
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if data is undefined
        if (localStorage.getItem('carthageArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // pushes data into array
          carthArray.push(changed1)
          romeArray.push(changed2)

          // saves data into localStorage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to array
          chart2.series[0].setData(carthArray)
          chart2.series[1].setData(romeArray)

          // otherwise retrieves data
        } else {
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 3) {
            romeArray.pop()
          }

          // pushes data into array
          romeArray.push(changed2)

          // saves data for later usage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets chart data to array
          chart2.series[1].setData(romeArray)

          // retrireves data from localStorage
          carthArray = JSON.parse(localStorage.getItem('carthageArray'))

          // removes inappropriate data
          while (carthArray.length > 3) {
            carthArray.pop()
          }

          // pushes data into array
          carthArray.push(null, changed1)

          // saves data to localStorage
          localStorage.setItem('carthageArray', JSON.stringify(carthArray))

          // sets data to array
          chart2.series[0].setData(carthArray)
        }


        // checks for next keywords
      } else if (chart.series[0].data[counter].name.includes("Mithridatic")) {

        // assigns data to vars
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if data is undefined
        if (localStorage.getItem('ponticArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 4) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (ponticArray.length > 3) {
            ponticArray.pop()
          }

          // pushes data to array
          ponticArray.push(null, null, null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('ponticArray', JSON.stringify(ponticArray))

          // sets chart data to array
          chart2.series[3].setData(ponticArray)
        }



        // checks for next keywords
      } else if (chart.series[0].data[counter].name.includes("Gallia")) {

        // assigns data to vars
        changed1 = parseInt(chart.series[0].data[counter].batData1)
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if data is undefined
        if (localStorage.getItem('gallicArray') == undefined || localStorage.getItem('romeArray') == undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 5) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)

          // removes inappropriate data
          while (gallicArray.length > 5) {
            gallicArray.pop()
          }

          // pushes data to array
          gallicArray.push(null, null, null, null, null, changed1)

          // saves data to localStorage
          localStorage.setItem('gallicArray', JSON.stringify(gallicArray))

          // sets chart data to array
          chart2.series[4].setData(gallicArray)
        }

      } else if (chart.series[0].data[counter].name.includes("Caesar")) {

        // assigns data to vars
        changed2 = parseInt(chart.series[0].data[counter].batData2)

        // if data is undefined
        if (localStorage.getItem('romeArray') != undefined) {

          // retrievs data from storage
          romeArray = JSON.parse(localStorage.getItem('romeArray'))

          // removes inappropriate data
          while (romeArray.length > 6) {
            romeArray.pop()
          }

          // pushes data to array
          romeArray.push(changed2)

          // saves data to localStorage
          localStorage.setItem('romeArray', JSON.stringify(romeArray))

          // sets data to array
          chart2.series[1].setData(romeArray)
        }

      }
    }
  }

  // Modifying the map and associated information via play button
  function cycle(counter) {
    if ($('#pause').val() == 'Pause Conquest') {

      var chart = $('#container').highcharts()
      var chart2 = $('#rightSide').highcharts()

      // updates map on a timer to prevent instant fill
      setTimeout(function() {

        chart.series[0].data[40 - counter].update({
          "color": 'red'
        }, true);
        chart.setTitle({
          text: chart.series[0].data[40 - counter].rtitle
        })
        chart.setTitle(null, {
          text: chart.series[0].data[40 - counter].year + "<br>" +
            chart.series[0].data[40 - counter].ruler
        })

        // updates chart to match map
        changeChart(counter)

        // disables pause if conquest finished
        if (counter <= 7) {
          counter = 0;
          disableButton('#pause')
        }

        // if counter is not 0, call function again
        if (--counter) cycle(counter);
      }, 5000)

      // if paused, save counter for later usage
    } else {
      counter = localStorage.setItem('PauseNumber', counter);
      alert("Conquest has been paused")
    }
  }



  // same as cycle but without the timeout
  // used in conjunction with map options
  function slowCycle(counter, stopNum) {

    // checks to see if a conquest has been paused
    if ($('#pause').val() == 'Pause Conquest' || $('#pause').val() == 'X') {

      var chart = $('#container').highcharts()

      // updates map
      chart.series[0].data[0 + counter].update({
        "color": 'red'
      }, true);
      chart.setTitle({
        text: chart.series[0].data[0 + counter].rtitle
      })
      chart.setTitle(null, {
        text: chart.series[0].data[0 + counter].year + "<br>" +
          chart.series[0].data[0 + counter].ruler
      })

      // changes chart to match map
      clickChart(counter)
      counter++;

      // checks to see if map has updated to appropriate stage
      if (counter == stopNum) {
        return false;
      } else {

        // if not, calls function again with incremented counter
        slowCycle(counter, stopNum);
      }
    } else {
      alert("You have currently paused a conquest cycle, please reset the map.")
    }
  }


  // Map options functionality
  $('#campList').on('change', function changeMap() {

    var counter = 1;
    var reset = 1;
    var stopNum;

    // disables incompatible functions
    disableAllButtons();
    $(this).prop('disabled', false)
    $(this).css('background-color', 'green')
    alert('The toggle, start conquest, and pause functions are disabled upon the usage of this feature, please reset the map to re-enable.')

    // resets map in case of prior alteration
    stopCycle(reset);

    // checks for desired area and calls map altering function based on val
    if ($(this).val() == 'latin') {
      stopNum = 2;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'pyrrhic') {
      stopNum = 4;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'firstPunic') {
      stopNum = 8;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'secondPunic') {
      stopNum = 9;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'thirdPunic') {
      stopNum = 16;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'greek') {
      stopNum = 11;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'mith') {
      stopNum = 19;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'gallia') {
      stopNum = 22;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'caesar') {
      stopNum = 23;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'augustus') {
      stopNum = 28;
      slowCycle(counter, stopNum)
    } else if ($(this).val() == 'trajan') {
      stopNum = 34;
      slowCycle(counter, stopNum)
    }
  })

  // iterates through chart data and updates their colour to red & changes
  // the chart's title/subtitle if appropriate using recursion
  $('#play').on('click', function startCycle() {

    // changes menu
    $('input[type=button]').css('background-color', '#504949')
    $(this).css('background-color', 'green');
    $('#pause').prop('disabled', false)
    $('#pause').val('Pause Conquest')
    $('#pause').css('background-color', '#504949')


    if (counter == undefined) {
      var counter = 40;

    }
    // calls map altering function using counter
    cycle(counter)
  })

  // Modifying the map and associated information via reset button
  function stopCycle(resetCounter) {

    var chart = $('#container').highcharts();
    var chart2 = $('#rightSide').highcharts();

    // clears the chart
    for (var i = 0; i < chart2.series.length; i++) {
      chart2.series[i].setData([
        []
      ], true)
    }
    // clears localStorage to prevent misuse of features
    localStorage.clear()

    // updates map to restore default state
    for (var i = 1; i < 40; i++) {

      chart.series[0].data[i].update({
        "color": 'gray'
      }, true);
      chart.setTitle({
        text: 'Roman Republic'
      })
      chart.setTitle(null, {
        text: '500 BCE'
      })
    }
  }

  // iterates through chart data and updates colour to gray using
  // recursion loop
  $('#stop').on('click', function resetCycle() {

    // resets menu to default state
    $('input[type=button]').css('background-color', '#504949')
    $('#campList').css('background-color', '#504949')
    $('input[type=button]').prop('disabled', false)
    $('#campList').val('default');
    $('#pause').prop('disabled', true)
    $('#pause').css('background-color', 'black')
    $('#chartCut').val('default')
    $('#play').val('Play Conquest')
    $('#toggleFor').val('Toggle Forward')
    $('#toggleBack').val('Toggle Backward')
    $('#toolBox').prop('disabled', false)
    disableButton('#pause')

    // resets map and chart
    var resetCounter = 1;
    stopCycle(resetCounter);
  })

  // pause button function
  $('#pause').on('click', function pauseCycle() {

    // changes buttons to defaults
    $('input[type=button]').css('background-color', '#504949')

    // changes colour to green to indicate use
    $(this).css('background-color', 'green');

    // disables incompatible feature
    disableButton('#play')

    // checks to see whether feature is paused
    if ($('#pause').val() == 'Resume Conquest') {
      $('#pause').val('Pause Conquest');

      // restarts conquest using stored counter
      cycle(localStorage.getItem('PauseNumber'));
    } else {
      $('#pause').val('Resume Conquest');

    }
  })

  // toggles the map forward by one Conquest
  $('#toggleFor').on('click', function forward() {
    counter = localStorage.getItem('PauseNumber');

    // disables play button
    disableButton('#play')

    // checks if counter exists
    if (counter == undefined) {
      counter = 40;
    } else if (counter == 6) {
      alert("Sorry, this feature cannot go further.")
      return false;
    }

    // calls toggle function using counter
    toggleForward(counter);
  })

  // takes counter from localStorage and iterates the chart once forward
  function toggleForward(counter) {

    var chart = $('#container').highcharts();

    // updates map according to counter
    chart.series[0].data[40 - counter].update({
      "color": 'red'
    }, true);
    chart.setTitle({
      text: chart.series[0].data[40 - counter].rtitle
    })
    chart.setTitle(null, {
      text: chart.series[0].data[40 - counter].year + "<br>" +
        chart.series[0].data[40 - counter].ruler
    })

    // changes chart to match map
    changeChart(counter)
    counter--;

    // saves counter into storage for future usage
    counter = localStorage.setItem('PauseNumber', counter);
  }

  // toggles the map backward by one conquest
  $('#toggleBack').on('click', function backward() {
    counter = localStorage.getItem('PauseNumber');
    if (counter == 40) {
      alert("Sorry, but this feature cannot go further! (Rome can't fall just yet)")
      return false;
    }

    toggleBackward(counter);
  })

  // takes counter from localStorage and interates once backwards
  function toggleBackward(counter) {

    var chart = $('#container').highcharts();

    // changes map according to counter
    chart.series[0].data[40 - counter].update({
      "color": 'gray'
    }, true);
    chart.setTitle({
      text: chart.series[0].data[40 - counter].rtitle
    })
    chart.setTitle(null, {
      text: chart.series[0].data[40 - counter].year + "<br>" +
        chart.series[0].data[40 - counter].ruler
    })

    // changes chart to match map
    changeChart(counter)
    counter++;

    // saves counter to storage for future usage
    counter = localStorage.setItem('PauseNumber', counter);
  }

  // disables buttons via taking element id
  function disableButton(title) {
    $(title).prop('disabled', true);
    $(title).css('background-color', 'black');
    $(title).val('X')
  }

  // disables all buttons via element id
  function disableAllButtons() {
    disableButton('#toggleFor')
    disableButton('#toggleBack')
    disableButton('#play')
    disableButton('#pause')
  }

  // function called when user clicks on map area
  function clickStart(index) {

    var counter = 0;
    var chart = $('#container').highcharts()

    // checks to see if the area is appropriate
    if (chart.series[0].data[index].name.includes('non')) {
      alert('Please select an appropriate province.')

      // disables incompatible features and calls map changing function
    } else {
      alert("Skipping to " + chart.series[0].data[index].name + " and disabling other features.")
      disableButton('#toggleFor')
      disableButton('#toggleBack')
      disableButton('#play')
      disableButton('#pause')
      disableButton('#campList')

      // resets the map and calls map changing function
      stopCycle(counter);
      clickCycle(index, counter)

    }

  }


  //  activates the conquest cycle but only up to the user's preference via clicking
  function clickCycle(index, counter) {

    // chart is the highchart
    var chart = $('#container').highcharts()

    // updates map until desired point
    chart.series[0].data[counter].update({
      "color": 'red'
    }, true);
    chart.setTitle({
      text: chart.series[0].data[counter].rtitle
    })
    chart.setTitle(null, {
      text: chart.series[0].data[counter].year + "<br>" +
        chart.series[0].data[counter].ruler
    })

    // changes chart to match map
    clickChart(counter)

    // increments counter for next iteration
    counter++;

    // if the counter doesn't match desired point, call function again
    if (counter != (index + 1)) {
      clickCycle(index, counter);
    }

  }



  // dialog functionality
  $(function() {

    // dialog options
    $("#dialog").dialog({
      autoOpen: false,
      modal: true,
      hide: {
        effect: "slide",
        duration: 1000
      },
      show: {
        effect: "slide",
        duration: 1000
      },
      height: 225,
      width: 600,

      // diff buttons for diff sources - think it makes it easier
      buttons: {
        Battles: function() {
          $(this).text("Battle data supplied by: Ancient History Encyclopedia - http://www.ancient.eu/ Ancient Atrocities - http://users.erols.com/mwhite28/romestat.htm")
        },
        Consuls: function() {
          $(this).text("Consul data supplied by: The Magistrates of the Roman Republic, Thomas Robert Shannon Broughton")
        },
        Emperors: function() {
          $(this).text("Emperors data supplied by: De Imperatoribus Romanis: An Online Encyclopedia of Roman Rulers and Their Families")
        },
        Highcharts: function() {
          $(this).text("Highchart choropleth map created using the Inkscape software and Highchart's SVG converter located at the following address:" +
            "\n https://www.highcharts.com/studies/map-from-svg.htm")
        },
        Close: function() {
          $(this).dialog("close")
        },
      },
      title: "Documentation and Citations",

      // dialog position
      position: {
        my: "center",
        at: "center"
      }
    });

    $("#documentation").click(function() {
      $("#dialog").dialog("open");
    });
  });


  // resize the divs according to screen
  $('body').css('min-height', screen.height);

  // Tooltips
  $(function() {
    $('#play').tooltip({
        content: "Proceeds from 500 BCE to 117 CE according to Rome's historical conquests.",
        track: false,
      }),

      $('#stop').tooltip({
        content: "Resets the map in order to replay the conquest.",
        track: false,
      }),

      $('#campList').tooltip({
        content: "Select an option to focus the charts on the preferred information.",
        track: false,
      }),

      $('#documentation').tooltip({
        content: "Displays information regarding the academic sources for the visual data.",
        track: false,
      }),

      $('#pause').tooltip({
        content: "Pauses the conquest if it is currently playing, or resumes it.",
        track: false,
      }),

      $('#toggleFor').tooltip({
        content: "Shifts the map forward by one conquest.",
        track: false,
      }),

      $('#toggleBack').tooltip({
        content: "Shifts the map backward by one conquest.",
        track: false,
      }),
      $('#chartCut').tooltip({
        content: "Change the view to focus only on map, chart, or both.",
        track: false,
      })
  });

  // resizes highcharts when screen diameters are affected
  $(window).resize(function() {

    chart1 = $('#container').highcharts()
    chart2 = $('#rightSide').highcharts()

    chart1.reflow()
    chart2.reflow()
  })

  // changes page view for map/chart only or both
  $('#chartCut').on('change', function() {

    chart1 = $('#container').highcharts()
    chart2 = $('#rightSide').highcharts()

    // if map option is chosen
    if ($('#chartCut').val() == 'map') {

      // changes css to hide chart and make map bigger
      $('#rightSide').css({
        'display': 'none'
      })
      $('#container').css({
        'display': 'block'
      })
      $('#container').css({
        'width': '83%'
      })
      $('#toolBox').prop('disabled', true)

      // refreshes the chart
      chart1.reflow()


      // if chart option is chosen
    } else if ($('#chartCut').val() == 'chart') {

      // changes css to hide map and make chart bigger
      $('#container').css({
        'display': 'none'
      })
      $('#rightSide').css({
        'display': 'block'
      })
      $('#rightSide').css({
        'width': '83%'
      })
      $('#rightSide').css({
        'margin-left': '11%'
      })

      // refreshes chart
      chart2.reflow()

      // if both is chosen
    } else if ($('#chartCut').val() == 'both' || $('#chartCut').val() == 'default') {

      // changes css to default values
      $('#rightSide').css({
        'display': 'initial'
      })
      $('#container').css({
        'display': 'initial'
      })
      $('#rightSide').css({
        'margin-left': '55%'
      })
      $('#rightSide').css({
        'width': '41.5%'
      })
      $('#container').css({
        'width': '42%'
      })
      $('#container').css({
        'margin-left': '11%'
      })
      $('#toolBox').prop('disabled', false)

      // refreshes both highcharts
      chart1.reflow()
      chart2.reflow()

    }
  })

});
