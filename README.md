Simply open the .html file in the Project Vis directory, and the project should be good to go.
The directories contain the following:
Roman Data is all of the information used in the creation of this project (historical maps, battle data, etc)
Roman Maps is the custom map that was made by myself for the purposes of this project. It was created using InkScape.
Project Vis is the actual code of the project.
All of the JS was kept in one file as a requirement of the assignment.

The project should be compatible with all browsers, with the exception of IE7 (Yeah..) It was primarily tested using Firefox 55.


Author: Charlie Murphy.
